from tests.config_test import client, connection

def test_should_status_code_ok(client):
    response = client.get('/')
    assert response.status_code == 200

def test_gest_post_id(connection):
    id = connection.execute('select id from posts where title = "second post"').fetchone()
    assert id[0] == 2

def test_gest_posts(connection):
    posts = connection.execute('select * from posts').fetchall()
    for row in posts:
        print(row)