import pytest
import sqlite3

from app import create_app


@pytest.fixture
def client():
    app = create_app()
    with app.test_client() as client:
        yield client

@pytest.fixture
def connection():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    yield cur