#!/usr/bin/python

import sqlite3

connection = sqlite3.connect('database.db')

with open('schema.sql') as f:
    connection.executescript(f.read())

cur = connection.cursor()

cur.execute("INSERT INTO posts (title, content) VALUES (?, ?)",
            ('premier post', 'contenu du premier post')
            )

cur.execute("INSERT INTO posts (title, content) VALUES (?, ?)",
            ('second post', 'contenu du second post')
            )

connection.commit()
connection.close()
