from flask import Flask, render_template, request, url_for, flash, redirect
import config
import sqlite3
# import pour page 404
from werkzeug.exceptions import abort

def get_db_connection():
    # attention, gérer les différents threads
    conn = sqlite3.connect('database.db', check_same_thread=False)
    conn.row_factory = sqlite3.Row
    return conn


def get_post(post_id):
    conn = get_db_connection()
    post = conn.execute('SELECT * FROM posts WHERE id = ?', (post_id,)).fetchone()
    conn.close()
    if post is None:
        abort(404)
    return post

def create_app():
    # créer une instance de l'app
    app = Flask(__name__)
    # ajouter les configurations
    app.config.from_object(config.DevConfig)

    # convertir les réponses en rep. http
    @app.route('/')
    def index():
        conn = get_db_connection()
        posts = conn.execute('SELECT * FROM posts').fetchall()
        conn.close()

        # render_template recup les modèles html présents dans le dossier des modèles
        return render_template('index.html', posts=posts)

    # post unique
    @app.route('/<int:post_id>')
    def post(post_id):
        print('id', post_id)
        post = get_post(post_id)
        print('post', post['id'])
        return render_template('post.html', post=post)

    @app.route('/create', methods=('GET', 'POST'))
    def create():
        title = ''
        if request.method == 'POST':
            title = request.form['title']
            content = request.form['content']
            print(title)
            if not title:
                flash('Saisir le titre')
            else:
                conn = get_db_connection()
                conn.execute('INSERT INTO posts (title, content) VALUES (?, ?)', (title, content))
                conn.commit()
                conn.close()
                return redirect(url_for('index'))
        return render_template('create.html')

    @app.route('/login', methods = ['POST', 'GET'])
    def login():
        if request.method == 'GET':
            return "Login via the login Form"
        
        if request.method == 'POST':
            password = request.form['password']
            if password == '123':
                #The following flash message will be displayed on successful login
                flash('Login successful')
                return render_template('success.html')
            else:
                return redirect('/form')

    @app.route('/<int:id>/edit', methods=('GET', 'POST'))
    def edit(id):
        post = get_post(id)

        if request.method == 'POST':
            title = request.form['title']
            content = request.form['content']

            if not title:
                flash('Title is required!')
            else:
                conn = get_db_connection()
                conn.execute('UPDATE posts SET title = ?, content = ?' ' WHERE id = ?', (title, content, id))
                conn.commit()
                conn.close()
                return redirect(url_for('index'))

        return render_template('edit.html', post=post)

    @app.route('/<int:id>/delete', methods=('POST',))
    def delete(id):
        post = get_post(id)
        conn = get_db_connection()
        conn.execute('DELETE FROM posts WHERE id = ?', (id,))
        conn.commit()
        conn.close()
        flash('"{}" a bien été supprimé !'.format(post['title']))
        return redirect(url_for('index'))

    return app