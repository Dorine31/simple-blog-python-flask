# créer un environnement
python3 -m venv flask-blog

# activer l'environnement
source flask-blog/bin/activate

# désactiver l'environnement
deactivate

# définir où se trouve l'application
FLASK_APP="src/app:create_app()"

# demarrer l'application
flask run

# lancer les tests
pytest